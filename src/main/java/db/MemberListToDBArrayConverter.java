package db;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.List;

@Converter(autoApply = true)
public class MemberListToDBArrayConverter implements AttributeConverter<List<Member>, String> {
    @Override
    public String convertToDatabaseColumn(List<Member> attribute) {
        if (null == attribute || 0 == attribute.size()) {
            return "{}";
        }

        StringBuilder sb = new StringBuilder("{");
        for (Member m : attribute) {
            sb.append(m.toDatabaseString());
            sb.append(",");
        }
        sb.setLength(sb.length() - 1);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public List<Member> convertToEntityAttribute(String dbData) {
        if (null == dbData || dbData.length() < 2 || dbData.charAt(0) != '{' || dbData.charAt(dbData.length() - 1) != '}') {
            throw new IllegalArgumentException("Argument cannot be parsed as array");
        }
        // TODO: implement
        return null;
    }
}
