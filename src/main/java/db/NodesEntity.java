package db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import osm.Node;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Entity @Table(name = "nodes")
@Getter @Setter @EqualsAndHashCode
public class NodesEntity {
    @Id private long id;
    @Basic private Double lat;
    @Basic private Double lon;
    @Basic private String username;
    @Basic private Long uid;
    @Basic private Boolean visible;
    @Basic private Long version;
    @Basic private Long changeset;
    @Basic private Timestamp timestamp;

    @Convert(converter = db.MapToHStoreConverter.class)
    @Column(columnDefinition = "hstore")
    private Map<String, String> tags = new HashMap<>();

    public NodesEntity() {}

    public NodesEntity(Node node) {
        id = node.getId().longValueExact();
        lat = node.getLat();
        lon = node.getLon();
        username = node.getUser();
        uid = node.getUid().longValueExact();
        visible = node.isVisible();
        version = node.getVersion().longValueExact();
        changeset = node.getChangeset().longValueExact();
        timestamp = new Timestamp(node.getTimestamp().toGregorianCalendar().getTimeInMillis());

        for (val tag : node.getTag()) {
            tags.put(tag.getK(), tag.getV());
        }
    }
}
