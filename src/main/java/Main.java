import db.NodesEntity;
import db.RelationsEntity;
import db.WaysEntity;
import osm.Node;
import osm.Relation;
import osm.Way;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.XMLEvent;
import java.io.FileReader;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("OsmPersistenceUnit");
        EntityManager em = emf.createEntityManager();

        OsmToDb(em, "data\\example.osm");

        em.close();
        emf.close();
    }

    private static void OsmToDb(EntityManager em, String fileName) {
        try {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLEventReader reader = inputFactory.createFilteredReader(
                    inputFactory.createXMLEventReader(new FileReader(fileName)),
                    new EventFilter() {
                        @Override
                        public boolean accept(XMLEvent event) {
                            if (!event.isStartElement() && !event.isEndElement()) return false;
                            QName elementName = (event.isStartElement() ?
                                    event.asStartElement().getName() : event.asEndElement().getName()
                            );
                            return elementIsNeeded(elementName.getLocalPart());
                        }

                        private boolean elementIsNeeded(String name) {
                            return !("osm".equals(name) || "bounds".equals(name));
                        }
                    }
            );

            JAXBContext jc = JAXBContext.newInstance("osm");
            Unmarshaller u = jc.createUnmarshaller();

            em.getTransaction().begin();

            int counter = 0;
            while (reader.hasNext()) {
                Object o = u.unmarshal(reader);
                Object entity = null;

                if (o instanceof Node) {
                    entity = new NodesEntity((Node) o);
                }
                else if (o instanceof Way) {
                    entity = new WaysEntity((Way) o);
                }
                else if (o instanceof Relation) {
                    entity = new RelationsEntity((Relation) o);
                }

                if (null != entity) {
                    em.persist(entity);
                    if (++counter % 10_000 == 0) {
                        System.out.println(counter);
                        em.flush();
                        em.clear();
                        break;
                    }
                }
            }

            em.getTransaction().commit();

            reader.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
