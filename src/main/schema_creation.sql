DROP TABLE Relations;
DROP TYPE Member;
DROP DOMAIN MemberType;
DROP TABLE Ways;
DROP TABLE Nodes;

CREATE TABLE Nodes (
	id BIGINT PRIMARY KEY,
	lat DOUBLE PRECISION,
	lon DOUBLE PRECISION,
	username TEXT,
	uid BIGINT,
	visible BOOLEAN,
	version BIGINT,
	changeset BIGINT,
	timestamp TIMESTAMP,

	tags hstore
);

CREATE TABLE Ways (
	id BIGINT PRIMARY KEY,
	username TEXT,
	uid BIGINT,
	visible BOOLEAN,
	version BIGINT,
	changeset BIGINT,
	timestamp TIMESTAMP,

  nodes BIGINT[],
	tags hstore
);

CREATE DOMAIN MemberType AS
	VARCHAR(8) CHECK (VALUE SIMILAR TO 'node|way|relation');

CREATE TYPE Member AS (
	type MemberType,
	ref BIGINT,
	role TEXT
);

CREATE TABLE Relations (
	id BIGINT PRIMARY KEY,
	username TEXT,
	uid BIGINT,
	visible BOOLEAN,
	version BIGINT,
	changeset BIGINT,
	timestamp TIMESTAMP,

	members Member[],
	tags hstore
);
